from app import db
from app.models import User, Post

u = User(username="John", email="john@example.com")
db.session.add(u)
db.session.commit()

u = User(username="Susan", email="susan@example.com")
db.session.add(u)
db.session.commit()

# creating a post for user 1
u = User.query.get(1)
p = Post(body="my forst post!", author=u)
db.session.add(p)
db.session.commit()
