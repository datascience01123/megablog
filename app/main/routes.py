from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required
from app.models import User
from datetime import datetime
from app.main import bp
from app.main.forms import EditProfileForm
from app import db


@bp.route("/seba")
def seba():
    return "seba!!!"


@bp.route("/")
@bp.route('/index')
@login_required
def index():
    # # fake news
    posts = [{
        "author": current_user,
        "body": "what a beautiful place"
    }, {
        "author": current_user,
        "body": "this is a great post about..."
    }]
    return render_template(
        "index.html", title="home", posts=posts)


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    # making temporary fake posts
    posts = [
        {"author": user, "body": "test post #1"},
        {"author": user, "body": "test post #2"},
    ]
    return render_template("user.html", user=user, posts=posts)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash("Your changes have been saved.")
        return redirect(url_for("main.edit_profile"))
    elif request.method == "GET":
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template(
        "edit_profile.html", title="Edit Profile", form=form)


@bp.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
